import NewMeetupForm from "../components/meetups/NewMeetupForm";
import { useHistory } from 'react-router-dom'
function NewMeetupPage() {
    const history = useHistory();
    function onAddMeetupHandler(meetupData) {
        fetch(
            'https://react-getting-started-44941-default-rtdb.firebaseio.com/meetups.json',
            {
                method : 'POST',
                body : JSON.stringify(meetupData),
                headers : {
                    'Content-Type' : 'application/json'
                }
            }
        ).then(() => {
            history.replace('/')
        });
    }
    return (
        <section>
            <h1>New Meetups Pages</h1>
            <NewMeetupForm onAddMeetup={onAddMeetupHandler}/>
        </section>
    )
}

export default NewMeetupPage;
