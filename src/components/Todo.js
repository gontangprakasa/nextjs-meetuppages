import { useState } from "react"
import Backdrop from "./Backdrop";
import Modal from "./Modal";

function Todo(props) {

    const [ modalIsOpen, setModalIsOpen ] = useState(false);
    function handleButton () {
        setModalIsOpen(true)
    }

    function handleCloseModal () {
        setModalIsOpen(false)
    }
    return (
        <div className='card'>
            <h2>{props.text}</h2>
            <div className="actions">
                <button className="btn" onClick={handleButton}>Delete</button>
            </div>

            { modalIsOpen && <Modal onCancel={handleCloseModal} onConfirm={handleCloseModal} /> }
            { modalIsOpen && <Backdrop onCencel={handleCloseModal} /> }
        </div>
    )
}

export default Todo;
