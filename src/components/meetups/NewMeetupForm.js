import clasess from "./NewMeetupForm.module.css"
import Card from "../ui/Card"
import {useRef} from "react"
function NewMeetupForm(props) {
    const titleInputRef = useRef();
    const imageInputRef = useRef();
    const addressInputRef = useRef();
    const descriptionInputRef = useRef();
    function submitHandler(event) {
        event.preventDefault();
        const enterTitle = titleInputRef.current.value;
        const enterImage = imageInputRef.current.value;
        const enterAddress = addressInputRef.current.value;
        const enterDescription = descriptionInputRef.current.value;

        const meetupData = {
            title : enterTitle,
            image : enterImage,
            address : enterAddress,
            description : enterDescription,
        };
        props.onAddMeetup(meetupData)
        
    }

    return (
        <Card>
            <form className={clasess.form} onSubmit={submitHandler}>
                <div className={clasess.control}>
                    <lable htmlFor="title">Meetup Title</lable>
                    <input type="text" required id="title" ref={titleInputRef} />
                </div>

                <div className={clasess.control}>
                    <lable htmlFor="image">Meetup Image</lable>
                    <input type="url" required id="image" ref={imageInputRef} />
                </div>

                <div className={clasess.control}>
                    <lable htmlFor="address">Address</lable>
                    <input type="text" required id="address" ref={addressInputRef}/>
                </div>

                <div className={clasess.control}>
                    <lable htmlFor="description">Description</lable>
                    <textarea type="text" required id="description" rows="5" ref={descriptionInputRef}> </textarea>
                </div>

                <div className={clasess.actions}>
                    <button>Add Meetup</button>
                </div>
            </form>
        </Card>
    )
}

export default  NewMeetupForm
