import clasess from "./MeetupItem.module.css"
import Card from "../ui/Card"
import { useContext,  } from 'react'
import FavoritesContext from "../../store/favorites-context"
function MeetupItem(props) {
    const favoritesCtx = useContext(FavoritesContext)
    const itemsIsFavorite = favoritesCtx.itemsIsFavorite(props.id)

    function toggleFavoritesStatusHandler() {
        if ( itemsIsFavorite ) {
            favoritesCtx.removeFavorite(props.id)
        } else {
            favoritesCtx.addFavorite({
                id : props.id,
                title : props.title,
                description : props.description,
                image : props.image,
                address : props.address
            })
        }
    }
    return (
        <li className={clasess.item}>
            <Card>
                <div className={clasess.image}>
                    <img src={props.image} alt={props.title}></img>
                </div>
                <div className={clasess.content}>
                    <h3>{props.title}</h3>
                    <address>{props.address}</address>
                    <p>{props.description}</p>
                </div>
                <div className={clasess.actions}>
                    <button onClick={toggleFavoritesStatusHandler}>{itemsIsFavorite ? 'Remove from Favorites' : ' To Favorites'}</button>
                </div>
            </Card>
        </li>
    )
}


export default MeetupItem